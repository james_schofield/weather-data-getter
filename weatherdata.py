#!python
"""
Tools to scrape weather data from wunderground.com.

Author: James Schofield
Year: 2016
Repo: https://bitbucket.org/james_schofield/weather-data-getter
"""


import argparse
import numpy as np
import pandas as pd

cols_float = ['TemperatureC', 'Dew_PointC', 'Humidity', 'Sea_Level_PressurehPa', 'VisibilityKm', 'Wind_SpeedKm_h',
              'Gust_SpeedKm_h', 'Precipitationmm', 'WindDirDegrees']

cols_string = ['Wind_Direction', 'Events', 'Conditions']

cols_keep = ['TemperatureC', 'Dew_PointC', 'Humidity', 'Sea_Level_PressurehPa', 'VisibilityKm',
             'WindDirDegrees', 'Wind_SpeedKm_h', 'Gust_SpeedKm_h',
             'Precipitationmm', 'Events', 'Conditions']


def _convert_to_float(s):
    try:
        return float(s)
    except:
        if s == '':
            return nan
        else:
            return 0.


def get_weather_data(location, date_from, date_to, verbose=True):
    """
    Returns a pandas DataFrame of weather data for the desired location and date range.
    :param location: must be recognisable in wunderground.com
    :param date_from: yyyy-mm-dd format
    :param date_to: yyyy-mm-dd format
    :return: pd.DataFrame

    Function example::
        get_weather_data('LTN', '2015-01-01', '2015-01-05', 'test.csv')

    Command line example::
        python weatherdata.py -l LTN -s 2015-01-01 -e 2015-01-05 -o test.csv
    """
    # make sure float and string column names are in the list of columns we want to keep
    ck = cols_keep
    cf = [c for c in cols_float if c in ck]
    cs = [c for c in cols_string if c in ck]

    daterange = pd.date_range(start=date_from, end=date_to, freq='D')  # test dates
    ndays = daterange.size

    # get data for each day in daterange
    days = []
    for i, day in enumerate(daterange):
        print('\rParsing day {}/{}: {}'.format(i+1, ndays, day.strftime('%Y-%m-%d')), end='\r')
        # url = 'http://www.wunderground.com/history/airport/LTN/{}/{}/{}/DailyHistory.html?format=1&units=metric'.format(day.year, day.month, day.day)
        url = 'http://www.wunderground.com/history/airport/{}/{}/{}/{}/DailyHistory.html?format=1&units=metric'.format(location, day.year, day.month, day.day)
        df = pd.read_csv(url, header=0)

        # in case it is an empty frame
        if df.index.size <= 1:
            continue

        # remove unwanted character
        df.columns = [c.replace('<br />', '').replace('/', '_').replace(' ', '_') for c in df.columns]
        df.DateUTC = df.DateUTC.map(lambda s: np.datetime64(s.replace('<br />', '')))
        df.set_index('DateUTC', inplace=True)
        df = df[cols_keep]
        df[cf] = df[cf].applymap(_convert_to_float)
        df[cs] = df[cs].fillna('')
        days.append(df)

    # in case no frames were taken
    if len(days) > 0:
        return pd.concat(days)[cols_keep]
    else:
        return None


def test():
    print('starting test')
    df = get_weather_data('LTN', '2014-01-01', '2014-01-05')
    if type(df) is pd.DataFrame:
        print('\nSeems to work fine.')
    else:
        print('\nSomething went wrong.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Tool for scraping weather data from wunderground.com. ')

    parser.add_argument('-t', '--test', action='store_true',
                        help='run test script')

    parser.add_argument('-l', '--location', type=str, default='LTN',
                        help='location: city name or airport code')

    parser.add_argument('-s', '--date_start', type=np.datetime64, default='2015-01-01',
                        help='beginning of date range [yyyy-mm--dd]')

    parser.add_argument('-e', '--date_end', type=np.datetime64, default='2015-01-05',
                        help='end of date range [yyyy-mm-dd]')

    parser.add_argument('-o', '--output_file', type=str, default='',
                        help='file path of output CSV file')

    args = parser.parse_args()

    if args.test:
        print('Test:')
        print('Getting weather at {} for dates {} to {}'.format(args.location, args.date_start, args.date_end))
        test()
    else:
        print('Getting weather at {} for dates {} to {}'.format(args.location, args.date_start, args.date_end))
        df = get_weather_data(args.location, args.date_start, args.date_end)

        if len(args.output_file) > 0:
            try:
                df.to_csv(args.output_file)
            except:
                print('\nCould not find weather data for this location and/or dates.')
